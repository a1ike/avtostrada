$(document).ready(function() {

  $('.phone').inputmask('+7(999)-999-99-99');

  $('.m-home-reviews__cards').slick({
    dots: false,
    arrows: true,
    infinite: true,
    speed: 500,
    slidesToShow: 2,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 4000,
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    }, {
      breakpoint: 900,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: false
      }
    } ]
  });

  $('.m-header__toggle').on('click', function(e) {
    e.preventDefault();
    $('.m-info').slideToggle('fast');
    $('.m-header__nav').slideToggle('fast');
  });

  $('.open-modal').on('click', function(e) {
    e.preventDefault();
    $('.m-modal').slideToggle('fast', function(e) {
      // callback
    });
  });

  $('.m-modal__close').on('click', function(e) {
    e.preventDefault();
    $('.m-modal').slideToggle('fast', function(e) {
      // callback
    });
  });

  new Swiper('.a-swiper', {
    autoplay: {
      delay: 4000,
    },
    loop: true,
    pagination: {
      el: '.a-swiper .swiper-pagination',
      clickable: true,
    },
  });

  $('.a-item__tabitem').click(function() {
    var tab_id = $(this).attr('data-tab');

    $('.a-item__tabitem').removeClass('a-item__tabitem_active');
    $('.a-item__tabcontent').removeClass('a-item__tabcontent_active');

    $(this).addClass('a-item__tabitem_active');
    $('#' + tab_id).addClass('a-item__tabcontent_active');
  });

  new Swiper('.a-papers__cards .swiper-container', {
    autoplay: {
      delay: 4000,
    },
    loop: true,
    slidesPerView: 4,
    spaceBetween: 30,
    navigation: {
      nextEl: '.a-papers__cards .swiper-button-next',
      prevEl: '.a-papers__cards .swiper-button-prev',
    },
    breakpoints: {
      1200: {
        slidesPerView: 1
      }
    }
  });

  $(window).on('resize load', function() {
    if ($(this).width() < 1200) {
      $('.m-header__dropper').on('click', function(e) {
        e.preventDefault();

        $(this).children().next().slideToggle('fast');
      });
    }
  });

});